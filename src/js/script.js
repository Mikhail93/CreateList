var numberOfLi = '';
var listDescription = [];

function setParamOfLi() {
    var modalWindow = document.createElement('div');
    modalWindow.className = 'modalWindow';

    var inputNumberOfLi = document.createElement('input');
    inputNumberOfLi.type = 'text';
    inputNumberOfLi.className = 'input-field d-block';
    inputNumberOfLi.placeholder = 'Input number of li';
    modalWindow.appendChild(inputNumberOfLi);

    var btnSubmitNumberOfLi = document.createElement('button');
    btnSubmitNumberOfLi.type = 'submit';
    btnSubmitNumberOfLi.className = 'btn btn-success btnSubmit d-block';
    btnSubmitNumberOfLi.innerHTML = 'Create';
    btnSubmitNumberOfLi.onclick = function () {
        numberOfLi = inputNumberOfLi.value;
        createList(numberOfLi, modalWindow);

    };
    modalWindow.appendChild(btnSubmitNumberOfLi);

    var param = document.getElementsByClassName('btn')[0];
    document.getElementById('container').replaceChild(modalWindow, param);
}

function createList(numberOfLi, replace) {
    var containerInput = document.createElement('div');
    containerInput.className = 'container-input';

    for (var i = 0; i < numberOfLi; i++) {
        var inputInfo = document.createElement("input");
        inputInfo.type = "text";
        inputInfo.className = 'input-field d-block';
        inputInfo.placeholder = 'Add content';
        containerInput.appendChild(inputInfo);
    }

    var btnCreateList = document.createElement('button');
    btnCreateList.type = 'submit';
    btnCreateList.className = 'btn btn-success btnSubmit d-block';
    btnCreateList.innerHTML = 'Create List';
    btnCreateList.onclick = function () {
        for (var j = 0; j < numberOfLi; j++) {
            var item = document.getElementsByTagName('input')[j];
            listDescription.push(item.value);
        }
        renderList(listDescription, containerInput, numberOfLi);
    };
    containerInput.appendChild(btnCreateList);

    document.getElementById('container').replaceChild(containerInput, replace);
}

function renderList(listDescription, replace, numberOfLi) {
    var containerList = document.createElement('div');
    containerList.className = 'container-list';

    var list = document.createElement('ul');
    containerList.appendChild(list);

    for (var k = 0; k < numberOfLi; k++) {
        var listItem = document.createElement('li');
        listItem.innerHTML = listDescription[k];
        list.appendChild(listItem);
    }

    var btnDelete = document.createElement('button');
    btnDelete.className = 'btn btn-danger';
    btnDelete.innerHTML = 'Delete';
    btnDelete.onclick = function () {
        reboot(containerList);
    };
    containerList.appendChild(btnDelete);

    document.getElementById('container').replaceChild(containerList, replace);
}

function reboot(containerList) {
    var btnStart = document.createElement('button');
    btnStart.type = 'button';
    btnStart.className = 'btn btn-warning';
    btnStart.onclick = function () {
        setParamOfLi();
    };
    btnStart.innerHTML = 'Start';

    document.getElementById('container').replaceChild(btnStart, containerList);
}
